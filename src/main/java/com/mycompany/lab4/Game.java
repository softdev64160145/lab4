/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.lab4;

/**
 *
 * @author user
 */
/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */

import java.util.Scanner;


/**
 *
 * @author Lenovo
 */
public class Game {

    private Player player1, player2;
    private table table;

    public Game() {
       player1 = new Player('X');
        player2 = new Player('O');
    }

    public void play() {
        boolean isFinish=false;
         printWelcome();
         newGame();
        while (!isFinish) {
            printTable();
            printTurn();
            inputRowCol();
            if (table.isWin()) {
                printTable();
                printWinner();
                printPlayers();
                isFinish=true;
            }
             if (table.isDraw()) {
                printTable();
                printDraw();
                printPlayers();
                isFinish=true;
        }
             table.swichPlayer();
    }
        inputContinue();
    }
    private void printWelcome() {
        System.out.println("Welcome to ox Game !!");
    }

    private void printTable() {
        char[][] t = table.getTable();
       for(int i=0; i<3;i++) {
            for(int j=0; j<3;j++) {
                System.out.print(t[ i ][ j ]+" ");
            }
            System.out.println("");
       }
    }

    private void printTurn() {
        System.out.println(table.getCurrentPlayer().getSymbol() + " Turn");
    }

    private void inputRowCol() {
        Scanner sc = new Scanner(System.in);
        System.out.print("Please in put row col :");
        int row = sc.nextInt();
        int col = sc.nextInt();
        table.setRowCol(row,col);
    }

    private void newGame() {
        table = new table(player1,player2);
    }

    private void printWinner() {
        System.out.println(table.getCurrentPlayer().getSymbol()+" Win !");
    }

    private void printDraw() {
        System.out.println("Draw");
    }
    private void printPlayers() {
        System.out.println(player1);
        System.out.println(player2);
    }

    private void inputContinue() {
        Scanner kb = new Scanner(System.in);
    while (true) {
        System.out.print("Do you want to play again? (y/n): ");
        String newGame = kb.next();
        if (newGame.equalsIgnoreCase("y")) {
            newGame();
            play();
            break;
        } else if (newGame.equalsIgnoreCase("n")) {
            System.out.println("Thank you for playing! Goodbye!");
            break;
        } else {
            System.out.println("Invalid input. Please enter 'y' or 'n'.");
        }
    }
    }
}

